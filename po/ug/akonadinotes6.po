# Uyghur translation for akonadinotes.
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Gheyret Kenji <gheyret@gmail.com>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: akonadinotes\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-06-25 00:38+0000\n"
"PO-Revision-Date: 2013-09-08 07:05+0900\n"
"Last-Translator: Gheyret Kenji <gheyret@gmail.com>\n"
"Language-Team: Uyghur <kde-i18n-doc@kde.org>\n"
"Language: ug\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: noteutils.cpp:373
#, kde-format
msgctxt "The default name for new notes."
msgid "New Note"
msgstr "يېڭى ئىزاھ"
