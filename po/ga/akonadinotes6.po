# Irish translation of akonadinotes
# Copyright (C) 2011 This_file_is_part_of_KDE
# This file is distributed under the same license as the akonadinotes package.
# Kevin Scannell <kscanne@gmail.com>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: akonadinotes\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-06-25 00:38+0000\n"
"PO-Revision-Date: 2011-12-28 12:28-0500\n"
"Last-Translator: Kevin Scannell <kscanne@gmail.com>\n"
"Language-Team: Irish <gaeilge-gnulinux@lists.sourceforge.net>\n"
"Language: ga\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=5; plural=n==1 ? 0 : n==2 ? 1 : n<7 ? 2 : n < 11 ? "
"3 : 4\n"

#: noteutils.cpp:373
#, kde-format
msgctxt "The default name for new notes."
msgid "New Note"
msgstr "Nóta Nua"
